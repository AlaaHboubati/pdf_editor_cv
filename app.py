from tkinter import *
from tkinter import filedialog
from tkinter import colorchooser
from PIL import  Image
from  pdf2image import convert_from_path
from  img2pdf import convert
import cv2
import os
import numpy as np
import  random
import pyautogui
import  time
from multiprocessing import  Pool

#-----------------------------------------------

original_images = []
current_images = []
output_dir= ""
screen_shot_dir = "screen_shots"
pdf_full_path = ""
text_masks = []
book_mark_path = "bookmark.png"
booked_mark_image_num = -1
booked_mark_image_name = "booked_mark_image.png"
eye_comfort_done = False

#------------------------------------------------

def eye_comfort():
    global  eye_comfort_done
    if eye_comfort_done:
        return
    eye_comfort_done = True
    global  current_images
    current_images_temp = []
    counter = 0

    for image in  current_images:
        result = np.copy(image)
        original_values = np.array([0, 50, 100, 150, 200, 255])
        red_values = np.array([0, 90, 170, 210, 240, 255])
        blue_values = np.array([0, 20, 40, 75, 130, 255])

        all_values = np.arange(0, 256)
        red_lookup_table = np.interp(all_values, original_values, red_values)

        blue_lookup_table = np.interp(all_values, original_values, blue_values)

        B,G,R = cv2.split(result)

        R = cv2.LUT(R, red_lookup_table)
        R = np.uint8(R)

        B = cv2.LUT(B, blue_lookup_table)
        B = np.uint8(B)

        result = cv2.merge([B, G, R])
        image_path = output_dir + os.path.basename(pdf_full_path).replace(".pdf","") + str(counter) + '.png'
        cv2.imwrite(image_path,result)
        current_images_temp.append(result)
        counter+=1
    current_images = np.copy(current_images_temp)

#-------------------------------------------------------


def unique_count_app(a):
    colors, count = np.unique(a.reshape(-1,a.shape[-1]), axis=0, return_counts=True)
    return colors[count.argmax()]

def change_background_color():
    global  original_images
    global  current_images
    global  text_masks
    color = colorchooser.askcolor()

    if  color[0] is None:
        return
    background_color = [color[0][2], color[0][1], color[0][0]]

    current_images = []
    counter = 0
    text_masks = []
    for image in original_images:
        result = np.copy(image)
        shape_image = np.zeros_like(image)

        gray_img = cv2.cvtColor(result,cv2.COLOR_BGR2GRAY)
        mask = cv2.adaptiveThreshold(gray_img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 25, 11)
        #_,mask = cv2.threshold(gray_img, 0, 255,cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        #_,mask = cv2.threshold(gray_img,180,255,cv2.THRESH_BINARY_INV)


        kernel = np.ones((2, 2), np.uint8)
        #mask = cv2.dilate(mask,kernel,iterations=2)


        _,contours,_ = cv2.findContours(mask,cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)
        i=0
        for contour in contours:

            if cv2.contourArea(contour) > 10000:
                    # using drawContours() function
                     cv2.fillPoly(shape_image, [contour],color= (255, 255, 255))

        # cv2.namedWindow("contours", cv2.WINDOW_NORMAL)
        # cv2.imshow("contours", shape_image)
        # cv2.waitKey(0)

        text_mask = np.zeros_like(image)
        text_mask[(mask == 255) & (shape_image[:, :, 0] != 255) & (shape_image[:, :, 1] != 255) &
               (shape_image[:, :, 2] != 255)] = [255,255,255]

        text_mask = cv2.dilate(text_mask,kernel,iterations=1)
        # cv2.namedWindow("text",cv2.WINDOW_NORMAL)
        # cv2.imshow("text",text_mask)
        # cv2.waitKey(0)

        text_masks.append(text_mask)

        result[(mask != 255)  & (shape_image[:,:,0] != 255) & (shape_image[:,:,1] != 255) &
               (shape_image[:,:,2] != 255)] = background_color
        result = cv2.erode(result, kernel, iterations=1)

        image_path = output_dir + os.path.basename(pdf_full_path).replace(".pdf", "") + str(counter) + '.png'
        cv2.imwrite(image_path, result)
        current_images.append(result)
        counter+=1

#-------------------------------------------

def calculate_text_masks():
    global  current_images
    global  text_masks
    for image in current_images:
        result = np.copy(image)
        shape_image = np.zeros_like(image)

        gray_img = cv2.cvtColor(result,cv2.COLOR_BGR2GRAY)
        mask = cv2.adaptiveThreshold(gray_img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 25, 11)

        kernel = np.ones((2, 2), np.uint8)
        #mask = cv2.dilate(mask,kernel,iterations=2)


        _,contours,_ = cv2.findContours(mask,cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)
        i=0
        for contour in contours:

            if cv2.contourArea(contour) > 7000:
                    # using drawContours() function
                     cv2.fillPoly(shape_image, [contour],color= (255, 255, 255))

        text_mask = np.zeros_like(image)
        text_mask[(mask == 255) & (shape_image[:, :, 0] != 255) & (shape_image[:, :, 1] != 255) &
               (shape_image[:, :, 2] != 255)] = [255,255,255]
        kernel = np.ones((2,2),np.uint8)
        text_mask = cv2.dilate(text_mask,kernel,iterations=1)

        text_masks.append(text_mask)


def change_text_color():
    global  original_images
    global  current_images
    global text_masks
    color = colorchooser.askcolor()
    if color[0] is None:
        return

    if len(text_masks) == 0:
        calculate_text_masks()

    text_color = [color[0][2], color[0][1], color[0][0]]
    current_images_temp=[]
    counter = 0
    for image in current_images:
        result = np.copy(image)
        text_mask = text_masks[counter]
        original_image = original_images[counter]
        lower_black = np.array([0, 0, 0])
        #upper_black = np.array([100, 255, 30])
        upper_black = np.array([100, 100, 200])
        hsv = cv2.cvtColor(original_image, cv2.COLOR_BGR2HSV)
        black_mask = cv2.inRange(hsv, lower_black, upper_black)


        result[(text_mask[:,:,0]== 255) & (text_mask[:,:,1]== 255) & (text_mask[:,:,2]== 255) &
               (black_mask == 255)]=text_color


        image_path = output_dir + os.path.basename(pdf_full_path).replace(".pdf", "") + str(counter) + '.png'
        cv2.imwrite(image_path,result)
        current_images_temp.append(result)
        counter += 1

    current_images = np.copy(current_images_temp)

#------------------------------------------------------------------------------

def save_pdf():
    save_path = filedialog.askdirectory(initialdir=os.getcwd())
    if not save_path:
        return
    images_to_be_converted = [str(output_dir+"/"+image_path) for image_path in os.listdir(output_dir)
                                                if image_path.endswith(".png")
                                                and image_path != booked_mark_image_name]

    save_name = str(random.randint(1,100000))+os.path.basename(pdf_full_path)
    with open(f'{save_path}/{save_name}','wb') as pdf_writer:
        pdf_writer.write(convert(images_to_be_converted))


def update_images():
    global  pdf
    global booked_mark_image_num
    update_booked_mark_image()
    if os.path.exists(output_dir):
        pdf.images = [PhotoImage(file=output_dir + "/" + image_path)
                      for image_path in os.listdir(output_dir) if image_path.endswith(".png") and
                      image_path != booked_mark_image_name]
        if booked_mark_image_num != -1:
            pdf.images[booked_mark_image_num] = PhotoImage(file=output_dir+"/"+booked_mark_image_name)


def show_images():
    global pdf
    pdf.delete('1.0', END)
    for image in pdf.images:
        pdf.image_create(END, image=image)
        pdf.insert(END, '\n\n')


def load_pdf():
    global pdf_full_path
    global output_dir
    global booked_mark_image_num
    global  eye_comfort_done
    global  text_masks
    pdf_full_path = filedialog.askopenfilename(
        initialdir=os.getcwd(),
        file=(
            ("PDF File",".pdf"),
            ("PDF File",".PDF")
        )
    )
    if pdf_full_path:
        booked_mark_image_num=-1
        text_masks = []
        eye_comfort_done = False
        pdf_name = os.path.basename(pdf_full_path)
        output_dir = pdf_name.replace(".pdf","") + "/"
        convert_pdf_to_images(output_dir,pdf_full_path)
        update_images()
        show_images()



def convert_pdf_to_images(output_dir,pdf_full_path):
    global  original_images
    global  current_images
    global  pdf
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    pages = convert_from_path(pdf_full_path,dpi=200,poppler_path=r'C:\Program Files\poppler-0.68.0\bin')
    counter = 0

    for page in pages:
        image_path = output_dir + os.path.basename(pdf_full_path).replace(".pdf","") + str(counter) + '.png'
        page.save(image_path)
        counter+=1

    original_images = [cv2.imread(str(os.path.join(output_dir,image_path)))
                       for image_path in os.listdir(output_dir) if image_path.endswith(".png") and
                       image_path!=booked_mark_image_name]
    current_images = np.copy(original_images)


#---------------------------------------------------------------

def update_booked_mark_image():
    global  booked_mark_image_num
    global booked_mark_image_name
    global current_images
    global  output_dir
    if booked_mark_image_num!=-1:
        image = np.copy(current_images[booked_mark_image_num])
        image = put_marker(image)
        cv2.imwrite(output_dir+"/"+booked_mark_image_name,image)


def double_click_event(event):
    global  booked_mark_image_num
    global pdf
    image_num = int(pdf.index(INSERT).split('.')[0])
    image_index = int(image_num/2)
    image_index = min(image_index,len(current_images)-1)
    if image_index == booked_mark_image_num:
        return
    booked_mark_image_num = image_index
    update_images()
    show_images()

def add_book_mark():
    global  booked_mark_image_num
    global  scroll_y
    image_index = int(scroll_y.get()[1]*len(original_images))
    image_index = min(image_index,len(current_images)-1)
    booked_mark_image_num = image_index

def put_marker(image):
    global  book_mark_path
    result = np.copy(image)
    logo = cv2.imread(book_mark_path)
    logo = cv2.resize(logo,(50,50),cv2.INTER_LANCZOS4)
    rows, cols, channels = logo.shape
    roi = image[0:rows, 0:cols]
    gray_logo = cv2.cvtColor(logo, cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(gray_logo, 2, 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)
    image_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)

    logo_fig = cv2.bitwise_and(logo, logo, mask=mask)

    dst = cv2.add(image_bg, logo_fig)
    result[0:rows, 0:cols] = dst

    return result

def find_book_mark():
    global  pdf
    if booked_mark_image_num != -1:
        pdf.yview(booked_mark_image_num*2)



#-------------------------------------------------------------

def scroll_vertically(value):
    # if value is positive it scrolls down else it scrolls up
    pdf.yview_scroll(value,"pixels")


def take_screen_shot():
    global  screen_shot_dir
    if not os.path.exists(screen_shot_dir):
        os.makedirs(screen_shot_dir)
    if not os.path.exists(screen_shot_dir+"/"+output_dir):
        os.makedirs(screen_shot_dir+"/"+output_dir)

    my_screen_shot = pyautogui.screenshot()
    screen_shot_path = screen_shot_dir + "/"+ output_dir+ "/" + str(random.randint(0,100000)) + ".png"
    my_screen_shot.save(screen_shot_path)

def open_camera():
    cam = cv2.VideoCapture(0)
    cv2.namedWindow("Camera",cv2.WINDOW_NORMAL)
    while True:
        _,frame = cam.read()
        cv2.imshow("Camera", frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        '''
            PROCESS FRAME HERE AND DECIDE WHETHER TO MOVE UP OR DOWN OR TAKE SCREENSHOT
            
        '''
        ## process frame and decide whether to move up or down or take screenshot

    cv2.destroyAllWindows()

#---------------------------------------------

if __name__ == "__main__":
    root = Tk()
    root.title("PDF Viewer")
    pdf_frame = Frame(root).pack(fill=BOTH, expand=True)
    scroll_y = Scrollbar(pdf_frame, orient=VERTICAL)
    scroll_x = Scrollbar(pdf_frame, orient=HORIZONTAL)
    pdf = Text(pdf_frame, yscrollcommand=scroll_y.set, xscrollcommand=scroll_x.set, bg="grey")
    pdf.bind('<Double-Button-1>', double_click_event)
    pdf.tag_configure("center", justify='center')

    scroll_y.pack(side=RIGHT, fill=Y)
    scroll_x.pack(side=BOTTOM, fill=X)
    scroll_x.config(command=pdf.xview)
    scroll_y.config(command=pdf.yview)
    pdf.pack(fill=BOTH, expand=True)

    change_background_color_button = Button(root,text="Change Background",command=lambda :[change_background_color(),
                                                                                           update_images(),show_images()])\
        .pack(padx=5, pady=15, side=LEFT,expand=True,fill=BOTH)

    change_text_color_button = Button(root,text="Change Text",command=lambda :[change_text_color(),
                                                                                           update_images(),show_images()])\
        .pack(padx=5, pady=15, side=LEFT,expand=True,fill=BOTH)

    eye_comfort_button = Button(root,text="Eye Comfort",command=lambda:[eye_comfort(),update_images(),show_images()])\
        .pack(padx=5, pady=15, side=LEFT,expand=True,fill=BOTH)

    load_pdf_button = Button(root,text="Load PDF",command=load_pdf)\
        .pack(padx=5, pady=15, side=LEFT,expand=True,fill=BOTH)

    save_pdf_button = Button(root,text="Save PDF",command=save_pdf)\
        .pack(padx=5, pady=15, side=LEFT,expand=True,fill=BOTH)

    bench_mark_button = Button(root,text="Book Mark",command=lambda:[add_book_mark(),update_images(),show_images()])\
        .pack(padx=5, pady=15, side=LEFT,expand=True,fill=BOTH)

    match_button = Button(root,text="Find Book Mark",command=find_book_mark)\
        .pack(padx=5, pady=15, side=LEFT,expand=True,fill=BOTH)

    '''
        this continues the current process and creates a new one, that's why we passed only 1 as the number of processes
        the second paramter is the function which you wana run on the second process
    '''
    pool = Pool(1,open_camera)

    root.mainloop()


    pool.terminate()